import * as React from "react";
import { View, Text } from "react-native";

import * as Google from "expo-google-app-auth";
import { Button } from "react-native-elements";
import * as firebase from "firebase";

const config = {
  scopes: ["profile", "email"],
  androidClientId: `579991813632-u2ofmqd71qleqt51s9326oqni46h959c.apps.googleusercontent.com`,
};

export default class Auth extends React.Component {
  constructor(props) {
    super(props);

    this.state = { load: false };
  }

  firebaselogin = async (googleUser) => {
    console.log("Google Auth Response", googleUser);
    // We need to register an Observer on Firebase Auth to make sure auth is initialized.
    var unsubscribe = firebase.auth().onAuthStateChanged((firebaseUser) => {
      if (firebaseUser) {
        firebase.auth().signOut();
        firebaseUser = null;
      }
      unsubscribe();
      console.log("hello1");
      // Check if we are already signed-in Firebase with the correct user.
      if (!this.isUserEqual(googleUser, firebaseUser)) {
        console.log("hello2");
        // Build Firebase credential with the Google ID token.
        var credential = firebase.auth.GoogleAuthProvider.credential(
          googleUser.idToken,
          googleUser.accessToken
        );
        // Sign in with credential from the Google user.
        firebase
          .auth()
          .signInWithCredential(credential)
          .then(async (res) => {
            console.log(res.additionalUserInfo);
            await this.setState({ load: false });

            if (res.additionalUserInfo.isNewUser) {
              this.props.navigation.navigate("Home", {
                userDetails: res.additionalUserInfo,
                status: "True",
              });
            } else {
              this.props.navigation.navigate("Home", {
                userDetails: res.additionalUserInfo,
                status: "False",
              });
            }
          })
          .catch(async (error) => {
            // Handle Errors here.
            console.log("error", error.message);
            await this.setState({ load: false });
            var errorCode = error.code;
            var errorMessage = error.message;
            // The email of the user's account used.
            var email = error.email;
            // The firebase.auth.AuthCredential type that was used.
            var credential = error.credential;
            // ...
          });
      } else {
        console.log("User already signed-in Firebase.");
      }
    });
  };

  isUserEqual = (googleUser, firebaseUser) => {
    if (firebaseUser) {
      var providerData = firebaseUser.providerData;
      for (var i = 0; i < providerData.length; i++) {
        if (
          providerData[i].providerId ===
            firebase.auth.GoogleAuthProvider.PROVIDER_ID &&
          providerData[i].uid === googleUser.user.id
        ) {
          // We don't need to reauth the Firebase connection.
          return true;
        }
      }
    }
    return false;
  };

  login = async () => {
    try {
      await this.setState({ load: true });
      const result = await Google.logInAsync(config);
      console.log(result);

      if (result.type === "success") {
        await this.firebaselogin(result);
      } else {
        await this.setState({ load: false });
      }
    } catch (e) {
      await this.setState({ load: false });
    }
  };

  render() {
    return (
      <View style={{ flex: 1, justifyContent: "center", alignItems: "center" }}>
        <Button
          title="Press me to login"
          loading={this.state.load}
          onPress={async () => {
            console.log("K");
            await this.login();
          }}
        ></Button>
      </View>
    );
  }
}
