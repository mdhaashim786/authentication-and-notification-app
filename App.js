import * as React from "react";
import { View, Text, Platform } from "react-native";
import * as Permissions from "expo-permissions";
import Constants from "expo-constants";
import { createAppContainer, createSwitchNavigator } from "react-navigation";
import { createStackNavigator } from "react-navigation-stack";

import * as Notifications from "expo-notifications";
import Screen1 from "./Screen1";
import * as firebase from "firebase";

import { Button } from "react-native-elements";
var notificationListener;
var responseListener;
const firebaseConfig = {
  apiKey: "AIzaSyAkkVcxXhc-wG4AzBtn0o31fYr5Sf7WIXg",
  authDomain: "realtime-project-83347.firebaseapp.com",
  databaseURL: "https://realtime-project-83347.firebaseio.com",
  projectId: "realtime-project-83347",
  storageBucket: "realtime-project-83347.appspot.com",
  messagingSenderId: "579991813632",
  appId: "1:579991813632:web:410aaa74b0fb71ed2562e5",
  measurementId: "G-Y19RWLRET5",
};
if (firebase.apps.length === 0) {
  console.log("hello");
  firebase.initializeApp(firebaseConfig);
}
Notifications.setNotificationHandler({
  handleNotification: async () => ({
    shouldShowAlert: true,
    shouldPlaySound: false,
    shouldSetBadge: false,
  }),
});

class HomeScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = { email: null, token: "" };
  }
  async componentDidMount() {
    const det = await this.props.navigation.getParam("userDetails");
    const status = await this.props.navigation.getParam("status");

    console.log(status);
    await this.setState({ email: det.profile.email, status: status });
    await this.registerForPushNotificationsAsync();
  }
  componentWillUnmount() {
    if (this.willFocusSubscription != undefined) {
      Notifications.removeNotificationSubscription(
        notificationListener.current
      );
      Notifications.removeNotificationSubscription(responseListener.current);
    }
  }
  sendPushNotification = async (token) => {
    const message = {
      to: token,
      sound: "default",
      title: "Munchin",
      body: "Click here to know about recipies",
      data: { data: "status" },
    };
    console.log("mynoteee");
    await fetch("https://exp.host/--/api/v2/push/send", {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Accept-encoding": "gzip, deflate",
        "Content-Type": "application/json",
      },
      body: JSON.stringify(message),
    });
  };
  registerForPushNotificationsAsync = async () => {
    let token;
    if (Constants.isDevice) {
      const {
        status: existingStatus,
      } = await Notifications.getPermissionsAsync();
      let finalStatus = existingStatus;
      if (existingStatus !== "granted") {
        const { status } = await Notifications.requestPermissionsAsync();
        finalStatus = status;
      }
      if (finalStatus !== "granted") {
        alert("Failed to get push token for push notification!");
        return;
      }
      token = (await Notifications.getExpoPushTokenAsync()).data;
      await this.setState({
        token: token,
      });
      console.log(token);
    } else {
      alert("Must use physical device for Push Notifications");
    }

    if (Platform.OS === "android") {
      Notifications.setNotificationChannelAsync("default", {
        name: "default",
        importance: Notifications.AndroidImportance.MAX,
        vibrationPattern: [0, 250, 250, 250],
        lightColor: "#FF231F7C",
      });
    }
    if (notificationListener != undefined) {
      notificationListener.current = () => {
        Notifications.addNotificationReceivedListener((notification) => {
          setNotification(notification);
        });
      };
    }
    if (responseListener != undefined) {
      // This listener is fired whenever a user taps on or interacts with a notification (works when app is foregrounded, backgrounded, or killed)
      responseListener.current = () => {
        Notifications.addNotificationResponseReceivedListener((response) => {
          console.log(response);
        });
      };
    }
  };

  render() {
    return (
      <View
        style={{
          flex: 1,
          alignItems: "center",
          justifyContent: "center",
          flexDirection: "column",
        }}
      >
        <View>
          <Button
            title="send notification"
            onPress={async () => {
              await this.sendPushNotification(this.state.token);
            }}
          ></Button>
        </View>
        <View style={{ marginTop: 30 }}>
          <Button
            title="Sign out"
            onPress={async () => {
              firebase.auth().signOut();
              this.props.navigation.navigate("My Application");
            }}
          ></Button>
        </View>
      </View>
    );
  }
}

class DetailsScreen extends React.Component {
  render() {
    return (
      <View style={{ flex: 1, alignItems: "center", justifyContent: "center" }}>
        <Text>Details Screen</Text>
      </View>
    );
  }
}

const RootStack = createStackNavigator(
  {
    Home: HomeScreen,
    Details: DetailsScreen,
  },
  {
    initialRouteName: "Home",
  }
);

const AuthStack = createStackNavigator({
  "My Application": Screen1,
});

const switcher = createSwitchNavigator(
  {
    AuthStack: AuthStack,
    RootStack: RootStack,
  },
  {
    initialRouteName: "AuthStack",
  }
);

const AppContainer = createAppContainer(switcher);

export default class App extends React.Component {
  render() {
    return <AppContainer />;
  }
}
